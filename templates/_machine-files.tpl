{{/*
Create the registry hosts .toml file
*/}}
{{- define "registry_mirrors" -}}
{{- $registryMirrors := get .Values "registry_mirrors" | default dict -}}
{{- $defaultSettings := get $registryMirrors "default_settings" | default dict -}}
{{- range $registry, $mirrors := get $registryMirrors "hosts_config" | default dict }}
- path: /etc/containerd/registry.d/{{ $registry }}/hosts.toml
  owner: root
  permissions: "0644"
  content: |
    server = "https://{{ $registry }}"
    {{- range $_, $config := $mirrors }}
    [host."{{ $config.mirror_url }}"]
    {{- range $setting,$value := mergeOverwrite (deepCopy $defaultSettings) (get $config "registry_settings" | default dict) }}
      {{ $setting }} = {{ $value | toJson }}
{{- end }}
{{- end }}
{{- end }}
{{ end }}

{{/*
Create the RKE2 containerd config .toml file
*/}}
{{- define "rke2_config_toml" -}}
- path: /var/lib/rancher/rke2/agent/etc/containerd/config.toml.tmpl
  owner: "root:root"
  permissions: "0640"
  encoding: base64
  # Template copied from https://github.com/k3s-io/k3s/blob/master/pkg/agent/templates/templates_linux.go
  # adapted to use /etc/containerd/registry.d directory for registry configuration (to be consistent with kubeadm)
  content: |
{{ .Files.Get "files/rke2.config.toml" | b64enc | indent 4 }}
{{- end }}

{{/*
Create the Kubernetes manifest for kubeadm VIP .yaml file
*/}}
{{- define "kubernetes_kubeadm_vip" -}}
- path: /etc/kubernetes/manifests/kube-vip.yaml
  owner: root:root
  permissions: "0644"
  content: |
    apiVersion: v1
    kind: Pod
    metadata:
      creationTimestamp: null
      name: kube-vip
      namespace: kube-system
    spec:
      containers:
      - args:
        - manager
        env:
        - name: svc_enable
          value: "true"
        - name: vip_arp
          value: "true"
        - name: port
          value: "6443"
        - name: vip_cidr
          value: "32"
        - name: cp_enable
          value: "true"
        - name: cp_namespace
          value: kube-system
        - name: vip_ddns
          value: "false"
        - name: vip_leaderelection
          value: "true"
        - name: vip_leaseduration
          value: "5"
        - name: vip_renewdeadline
          value: "3"
        - name: vip_retryperiod
          value: "1"
        - name: address
          value: {{ .Values.cluster_virtual_ip }}
        {{- if .Values.kube_vip.bgp_lbs }}
        - name: bgp_enable
          value: "true"
        {{- range $key, $value := .Values.kube_vip.bgp_lbs }}
        - name: {{ $key }}
          value: {{ $value }}
        {{- end }}
        {{- end }}
        - name: prometheus_server
          value: :2112
        # Renovate Bot needs additional information to detect the kube-vip version:
        # renovate: registryUrl=https://ghcr.io image=kube-vip/kube-vip
        image: {{ .Values.images.kube_vip.repository }}:{{ .Values.images.kube_vip.tag }}
        imagePullPolicy: Always
        name: kube-vip
        resources: {}
        securityContext:
          capabilities:
            add:
            - NET_ADMIN
            - NET_RAW
        volumeMounts:
        - mountPath: /etc/kubernetes/admin.conf
          name: kubeconfig
      hostAliases:
      - hostnames:
        - kubernetes
        ip: 127.0.0.1
      hostNetwork: true
      volumes:
      - hostPath:
          path: /etc/kubernetes/admin.conf
        name: kubeconfig
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 MetalLB .yaml file
*/}}
{{- define "kubernetes_rke2_metallb" -}}
- path: /var/lib/rancher/rke2/server/manifests/metallb.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
      ---
      apiVersion: v1
      kind: Namespace
      metadata:
        name: metallb-system
        labels:
          pod-security.kubernetes.io/enforce: privileged
          pod-security.kubernetes.io/audit: privileged
          pod-security.kubernetes.io/warn: privileged
      ---
      apiVersion: helm.cattle.io/v1
      kind: HelmChart
      metadata:
        name: metallb
        namespace: metallb-system
      spec:
        bootstrap: true
        chart: {{ .Values.metallb_helm_oci_url | default "metallb" }}
        repo: {{ empty .Values.metallb_helm_oci_url | ternary "https://metallb.github.io/metallb" "" }}
        version: {{ .Values.metallb_helm_version }}
        targetNamespace: metallb-system
        {{- if .Values.metallb_helm_extra_ca_certs }}
        repoCA: |- {{ .Values.metallb_helm_extra_ca_certs | nindent 10 }}
        {{- end }}
        valuesContent: |-
          controller:
            nodeSelector:
              node-role.kubernetes.io/control-plane: "true"
            tolerations:
            - key: node.cloudprovider.kubernetes.io/uninitialized
              value: "true"
              effect: NoSchedule
            - effect: NoExecute
              key: node-role.kubernetes.io/etcd
            - effect: NoSchedule
              key: node-role.kubernetes.io/master
            - effect: NoSchedule
              key: node-role.kubernetes.io/control-plane
          speaker:
            frr:
              enabled: false
            nodeSelector:
              node-role.kubernetes.io/control-plane: "true"
            tolerations:
            - key: node.cloudprovider.kubernetes.io/uninitialized
              value: "true"
              effect: NoSchedule
            - effect: NoExecute
              key: node-role.kubernetes.io/etcd
            - effect: NoSchedule
              key: node-role.kubernetes.io/master
            - effect: NoSchedule
              key: node-role.kubernetes.io/control-plane
      ---
      apiVersion: metallb.io/v1beta1
      kind: IPAddressPool
      metadata:
        name: lbpool
        namespace: metallb-system
      spec:
        addresses:
          - {{ .Values.cluster_virtual_ip }}/32
      ---
      apiVersion: metallb.io/v1beta1
      kind: L2Advertisement
      metadata:
        name: l2advertisement
        namespace: metallb-system
      spec:
        ipAddressPools:
        - lbpool
{{- if eq .Values.capi_providers.infra_provider "capm3" }}
        interfaces: {{ .Values.cluster_primary_interfaces | default (list .Values.control_plane.capm3.primary_pool_interface) | required "we need one of: .cluster_primary_interfaces, control_plane.capm3.primary_pool_interface" | toYaml | nindent 10 }}
{{- end }}
{{- if .Values.metallb.l2_lbs }}
  {{- range .Values.metallb.l2_lbs.address_pools }}
      ---
      apiVersion: metallb.io/v1beta1
      kind: IPAddressPool
      metadata:
        name: {{ .name }}
        namespace: metallb-system
      spec:
        addresses: {{ .addresses | toYaml | nindent 8 }}
  {{- end -}}
  {{- range .Values.metallb.l2_lbs.l2_options.advertisements }}
      ---
      apiVersion: metallb.io/v1beta1
      kind: L2Advertisement
      metadata:
        name: {{ .name }}
        namespace: metallb-system
      spec:
        ipAddressPools: {{ .advertised_pools | toYaml | nindent 8 }}
        {{- if .node_selectors }}
        node_selectors: {{ .node_selectors | toYaml | nindent 8 }}
        {{- end }}
        {{- if .interface }}
        interface: {{ .interface }}
        {{- end }}
  {{- end }}
{{- end }}
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 MetalLB-L3 .yaml file
*/}}
{{- define "kubernetes_rke2_metallb_l3" -}}
{{- if .Values.metallb.bgp_lbs }}
- path: /var/lib/rancher/rke2/server/manifests/metallb-l3.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
    {{- range .Values.metallb.bgp_lbs.address_pools }}
      ---
      apiVersion: metallb.io/v1beta1
      kind: IPAddressPool
      metadata:
        name: {{ .name }}
        namespace: metallb-system
      spec:
        addresses: {{ .addresses | toYaml | nindent 8 }}
    {{- end -}}
    {{- range .Values.metallb.bgp_lbs.l3_options.bgp_peers }}
      ---
      apiVersion: metallb.io/v1beta2
      kind: BGPPeer
      metadata:
        name: {{ .name }}
        namespace: metallb-system
      spec:
        myASN: {{ .local_asn }}
        peerASN: {{ .peer_asn }}
        peerAddress: {{ .peer_address }}
      ---
      apiVersion: metallb.io/v1beta1
      kind: BGPAdvertisement
      metadata:
        name: {{ .name }}
        namespace: metallb-system
      spec:
        ipAddressPools: {{ .advertised_pools | toYaml | nindent 8 }}
        peers:
        - {{ .name }}
    {{- end }}
{{- end }}
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 VIP .yaml file
*/}}
{{- define "kubernetes_rke2_vip" -}}
- path: /var/lib/rancher/rke2/server/manifests/kubernetes-vip.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
      ---
      apiVersion: v1
      kind: Service
      metadata:
        name: kubernetes-vip
        namespace: kube-system
        annotations:
          metallb.universe.tf/allow-shared-ip: cluster-external-ip
      spec:
        type: LoadBalancer
        loadBalancerIP: {{ .Values.cluster_virtual_ip }}
        ports:
        - name: https
          port: 6443
          protocol: TCP
          targetPort: 6443
        - name: rancher
          port: 9345
          protocol: TCP
          targetPort: 9345
        selector:
          component: kube-apiserver
{{- end }}

{{/*
Create the resolv.conf file
*/}}
{{- define "resolv_conf" -}}
{{- if eq .Values.capi_providers.infra_provider "capd" }}
- path: /etc/resolv.conf
  owner: root
  permissions: "0644"
  content: |
    nameserver {{ include "rancher_dns_resolver" . }}
{{- else }}
- path: /etc/systemd/resolved.conf
  content: |
    [Resolve]
    DNS={{ include "rancher_dns_resolver" . }}
    Domains=~{{ .Values.rancher_domain }}
{{- end }}
{{- end }}

{{/*
Create the containerd proxy.conf file
*/}}
{{- define "containerd_proxy_conf" }}
- path: /etc/systemd/system/containerd.service.d/proxy.conf
  owner: root
  permissions: "0644"
  content: |
    [Service]
    Environment="HTTP_PROXY={{ .Values.proxies.http_proxy }}"
    Environment="HTTPS_PROXY={{ .Values.proxies.https_proxy }}"
    Environment="NO_PROXY={{ .Values.proxies.no_proxy }}"
{{- end }}

{{/*
Create the RKE2-server containerd proxy settings file
*/}}
{{- define "rke2_server_containerd_proxy" }}
- path: /etc/default/rke2-server
  owner: root:root
  permissions: "0644"
  content: |
    HTTP_PROXY={{ .Values.proxies.http_proxy }}
    HTTPS_PROXY={{ .Values.proxies.https_proxy }}
    NO_PROXY={{ .Values.proxies.no_proxy }}
{{- end }}

{{/*
Create the RKE2-agent containerd proxy settings file
*/}}
{{- define "rke2_agent_containerd_proxy" }}
- path: /etc/default/rke2-agent
  owner: root:root
  permissions: "0644"
  content: |
    HTTP_PROXY={{ .Values.proxies.http_proxy }}
    HTTPS_PROXY={{ .Values.proxies.https_proxy }}
    NO_PROXY={{ .Values.proxies.no_proxy }}
{{- end }}

{{/*
Configure rke2-calico HelmChart to tolerate node.cloudprovider.kubernetes.io/uninitialized and enable wireguard
*/}}
{{- define "rke2_calico_helm_chart_config" }}
- path: /var/lib/rancher/rke2/server/manifests/rke2-calico-config.yaml
  owner: root:root
  permissions: "0644"
  content: |
    apiVersion: helm.cattle.io/v1
    kind: HelmChartConfig
    metadata:
      name: rke2-calico
      namespace: kube-system
    spec:
      valuesContent: |-
        {{- if .Values.cni.calico.wireguard_enabled }}
        felixConfiguration:
          wireguardEnabled: true
        {{- end }}
        installation:
          controlPlaneTolerations:
            - key: "node.cloudprovider.kubernetes.io/uninitialized"
              effect: "NoSchedule"
              value: "true"
          calicoNetwork:
        {{- if .Values.capi_providers.infra_provider | eq "capm3" }}
            mtu: {{ .Values.capm3.network_interfaces.mtu }}
        {{- end }}
            nodeAddressAutodetectionV4:
              {{- if .Values.cni.calico.autodetection_method }}
              {{ .Values.cni.calico.autodetection_method | toYaml }}
              {{ else }}
               canReach: {{ not (empty .Values.mgmt_cluster_ip) | ternary .Values.mgmt_cluster_ip .Values.cluster_virtual_ip }}
              {{- end }}
{{- end }}

{{/*
Configure rke2-coredns HelmChart to tolerate node.cloudprovider.kubernetes.io/uninitialized
*/}}
{{- define "rke2_coredns_helm_chart_config" }}
- path: /var/lib/rancher/rke2/server/manifests/rke2-coredns-toleration.yaml
  owner: root:root
  permissions: "0644"
  content: |
    apiVersion: helm.cattle.io/v1
    kind: HelmChartConfig
    metadata:
      name: rke2-coredns
      namespace: kube-system
    spec:
      valuesContent: |-
        tolerations:
          - key: "node.cloudprovider.kubernetes.io/uninitialized"
            effect: "NoSchedule"
            value: "true"
{{- end }}

{{/*
Configure rke2-kubelet-config-file
*/}}
{{- define "rke2-kubelet-config-file" }}
- path: /var/lib/rancher/rke2/server/kubelet-configuration-file.yaml
  owner: root:root
  permissions: "0644"
  content: |
    ---
    apiVersion: kubelet.config.k8s.io/v1beta1
    kind: KubeletConfiguration
    {{ if . }}
    {{ . | toYaml | nindent 4 }}
    {{ end }}
{{- end }}


{{/*
Create the machine files defined from external files through chart values .additional_files
*/}}
{{- define "additional_files" -}}
{{- $envAll := index . 0 -}}
{{- $additional_files := index . 1 -}}
{{- range $file, $file_specs := $additional_files }}
- owner: {{ $file_specs.owner }}
  permissions: {{ $file_specs.permissions | quote }}
  {{- if $file_specs.path }}
  path: {{ $file_specs.path }}
  {{- else }}
  path: {{ $file }}
  {{- end }}
  {{- if $file_specs.content }}
    {{- if $file_specs.encoding }}
  encoding: {{ $file_specs.encoding }}
  content: |
{{ $file_specs.content | indent 4 }}
    {{- else }}
      {{/*********** protect $file_specs.content from unwanted evaluation
      Helm supports only Base64 & Base32 encoding, https://helm.sh/docs/chart_template_guide/function_list/#encoding-functions
      out of which CABPK & CABPR only support base64 */}}
  encoding: base64
  content: |
{{ $file_specs.content | b64enc | indent 4 }}
    {{- end }}
  {{- else if $file_specs.content_file }}
    {{- if not ($envAll.Files.Glob ($file_specs.content_file | clean)) }}
      {{- fail (printf "no such file exists: %s" $file_specs.content_file) -}}
    {{- end }}
    {{- if $file_specs.encoding }}
  encoding: {{ $file_specs.encoding }}
  content: |
{{ $envAll.Files.Get $file_specs.content_file | indent 4 }}
    {{- else }}
      {{/*********** protect $file_specs.content_file contents from unwanted evaluation
      Helm supports only Base64 & Base32 encoding, https://helm.sh/docs/chart_template_guide/function_list/#encoding-functions
      out of which CABPK & CABPR only support base64 */}}
  encoding: base64
  content: |
{{ $envAll.Files.Get $file_specs.content_file | b64enc | indent 4 }}
    {{- end }}
  {{- else if $file_specs.content_k8s_secret }}
  contentFrom:
    secret: {{ $file_specs.content_k8s_secret | toJson }}
  {{- end }}
{{- end }}
{{ end }}


{{/*
Create the kubernetes audit policy file defined from  policy rules defines in user values
*/}}
{{- define "audit_policy_config_file" -}}
{{ if .Values.audit_policies }}
- path: /etc/rancher/rke2/audit-policy.yaml
  owner: root:root
  permissions: "0400"
  content: |
    apiVersion: audit.k8s.io/v1
    kind: Policy
    {{ if .Values.audit_policies.omitStages }}
    omitStages: {{ .Values.audit_policies.omitStages | toYaml | nindent 4 }}
    {{- end }}
    rules:
    {{ .Values.audit_policies.rules | toYaml | trim | nindent 4 }}
{{- end }}
{{- end }}
