#!/usr/bin/env bash

set -e
set -o pipefail

HELM_NAME=${HELM_NAME:-$1}

if [[ -z ${HELM_NAME} ]]; then
  echo "Missing parameter.

  This script expect to find either:

  HELM_NAME environment variable defined with the name of the chart to validate

  or the name of the chart to validate pass as a parameter.

  helm-template-yamllint.sh sylva-capi-cluster

  "
  exit 1
fi

function helm() { $(which helm) $@ 2> >(grep -v 'found symbolic link' >&2); }

export BASE_DIR=$( cd "$(dirname "${BASH_SOURCE[0]}")/../.." ; pwd -P )

chart_dir=${BASE_DIR}

echo -e "\e[0Ksection_start:`date +%s`:helm_dependency_build\r\e[0K--------------- helm dependency build"
helm dependency build $chart_dir
echo -e "\e[0Ksection_end:`date +%s`:helm_dependency_build\r\e[0K"

test_dirs=$(find $chart_dir/test-values -mindepth 1 -maxdepth 1 -type d)
if [ -d $chart_dir/test-values ] && [ -n "$test_dirs" ] ; then
  for dir in $test_dirs ; do
    echo -e "\e[0Ksection_start:`date +%s`:helm_more_values\r\e[0K--------------- Checking values from test-values/$(basename $dir) with 'helm template' and 'yamllint' ..."

    set +e
    helm template ${HELM_NAME} $chart_dir $(ls $dir/*.y*ml | grep -v test-spec.yaml | sed -e 's/^/--values /') \
      | yamllint - -d "$(cat < ${BASE_DIR}/.gitlab/configuration/yamllint.yaml) $(cat < ${BASE_DIR}/.gitlab/configuration/yamllint-helm-template-rules)"
    exit_code=$?
    set -e

    if [[ -f $dir/test-spec.yaml && $(yq .require-failure $dir/test-spec.yaml) == "true" ]]; then
        expected_exit_code=1
        error_message="ERROR - This testcase is supposed to make 'helm template ..| yamllint ..' fail, but it actually succeeded."
        success_message="OK - This negative testcase expectedly made 'helm template ..| yamllint ..' fail."
    else
        expected_exit_code=0
        error_message="ERROR - Failure when running 'helm template ..| yamllint ..' on this test case."
        success_message="OK"
    fi

    if [[ $exit_code -ne $expected_exit_code ]]; then
      echo $error_message
      exit -1
    else
      echo $success_message
    fi

    echo -e "\e[0Ksection_end:`date +%s`:helm_more_values\r\e[0K"
  done
fi
