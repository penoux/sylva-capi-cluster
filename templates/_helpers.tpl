{{/*
Expand the name of the chart.
*/}}
{{- define "sylva-capi-cluster.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "sylva-capi-cluster.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "sylva-capi-cluster.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "sylva-capi-cluster.labels" -}}
helm.sh/chart: {{ include "sylva-capi-cluster.chart" . }}
{{ include "sylva-capi-cluster.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "sylva-capi-cluster.selectorLabels" -}}
app.kubernetes.io/name: {{ include "sylva-capi-cluster.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}


{{- define "containerd-config.toml-registry-config" }}
c=/etc/containerd/config.toml

# Remove default mirroring configuration for k8s.gcr.io as it can't coexist with registry config dir
sed -i '/k8s.gcr.io/d' $c
if ! grep -q "config_path *=.*/etc/containerd/registry.d" $c; then
  cp $c $c.orig
  if ! grep -q  '"io.containerd.grpc.v1.cri".registry\]' $c ; then
    # we add the missing section
    echo '[plugins."io.containerd.grpc.v1.cri".registry]' >> $c
    echo '  config_path = "/etc/containerd/registry.d"' >> $c
  else
    # we assume that it's a recent containerd config file, already having config_path set for this section
    sed -i -e '/io.containerd.grpc.v1.cri".registry\]/ { n; s|config_path.*|config_path = "/etc/containerd/registry.d"| }' $c
  fi
fi
systemctl daemon-reload
systemctl restart containerd.service
{{ end -}}

{{/*
Get Metal3MachineTemplate.spec and return it after replacing the URL host part with an empty string.
For determining Metal3MachineTemplate.metadata.name we need to compute the hash of Metal3MachineTemplate.spec content
without the IP/hostname part in m3mt.spec.template.spec.image.checksum & m3mt.spec.template.spec.image.url,
to avoid useless node rolling upgrade after pivot, see https://gitlab.com/sylva-projects/sylva-core/-/issues/621
*/}}
{{- define "Metal3MachineTemplateSpec-remove-url-hostname" }}
{{- $m3mt_spec := fromYaml . -}}
{{- $m3mt_image_checksum := $m3mt_spec.template.spec.image.checksum -}}
{{- if (hasPrefix "http" $m3mt_spec.template.spec.image.checksum) -}}
  {{- $m3mt_image_checksum = urlParse $m3mt_spec.template.spec.image.checksum -}}
  {{- $_ := set $m3mt_image_checksum "host" "" -}}
  {{- $m3mt_image_checksum = urlJoin $m3mt_image_checksum -}}
{{- end -}}
{{- $m3mt_image_url := urlParse $m3mt_spec.template.spec.image.url -}}
{{- $_ := set $m3mt_image_url "host" "" -}}
{{- $m3mt_image_url = urlJoin $m3mt_image_url -}}
{{- $m3mt_spec_replaced := mergeOverwrite (deepCopy $m3mt_spec) (dict "template" (dict "spec" (dict "image" (dict "checksum" $m3mt_image_checksum
                                                                                                                  "url"      $m3mt_image_url))))
-}}
{{- $m3mt_spec_replaced | toYaml -}}
{{- end -}}


{{- define "shell-longhorn-mounts" -}}
# handle longhorn disk mounts defined via Metal3DataTemplate based on BareMetalHost annotations
sylva_longhorn_disks_base64e="{{`{{ds.meta_data.sylva_longhorn_disks}}`}}"
sylva_longhorn_disks=$(echo "$sylva_longhorn_disks_base64e" | base64 -d)

if [ -n "$sylva_longhorn_disks" ] ; then
  echo "processing Longhorn disk mounts defined by sylvaproject.org/default-longhorn-disks-config BMH annotation"
  disk_mount_paths=$(echo "$sylva_longhorn_disks" | jq -r '.[].path')
  for mnt in $disk_mount_paths; do
    disk=$(echo "$mnt" | awk -F"/var/longhorn/disks/" '{print $2}' | tr _ /)
    dev=/dev/$disk
    echo "longhorn disk $dev -> $mnt"

    # partition disk if not already partitioned
    if [ -z "$(lsblk "$dev" -o FSTYPE -n)" ]; then
      mkfs.ext4 $dev
    else
      echo "$dev is not empty:"
      lsblk "$dev"
    fi

    mkdir -p $mnt
    echo "$dev $mnt ext4 defaults 0 0" >> /etc/fstab
    mount --target $mnt
  done
fi
{{- end -}}


{{/*
merge-append template: like mergeOverwrite, but append lists instead of overwriting them.

Usage:

    tuple $dst $src | include "merge-append"

Values:

    dest:
      one: thing
      two:
       - a
       - b

    source1:
      one: stuff
      two:
       - a
       - c

    source2:
      one: other
      two:
       - d

Template:

{{- tuple .Values.dest .Values.source1 .Values.source2 | include "merge-append" }}
{{ .Values.dest | toYaml }}

Result:

    one: other
    two:
    - a
    - b
    - a
    - c
    - d

*/}}
{{- define "merge-append" }}
    {{- $dst := first . }}
    {{- range $src := rest . }}
        {{- $dst := tuple $dst $src | include "_merge-append" }}
    {{- end }}
{{- end }}

{{- define "_merge-append" }}
    {{- $dst := index . 0 }}
    {{- $src := index . 1 }}
    {{- if not (kindIs "map" $src) }}
        {{- fail (printf "'merge-append' called with a source object which is not a map (<%s> is %s)" ($src | toString) (kindOf $src)) }}
    {{- end }}
    {{- if not (kindIs "map" $dst) }}
        {{- fail (printf "'merge-append' called with a source object which is not a map (<%s> is %s)" ($dst | toString) (kindOf $dst)) }}
    {{- end }}
    {{- $result := $dst }}
    {{- range $key,$value := $src }}
        {{- if (hasKey $dst $key) }}
            {{- if (eq (kindOf $value) (kindOf (get $dst $key))) }}
                {{- if (eq (kindOf $value) "slice") }}
                    {{- /* append src list to dst one */}}
                    {{- $_ := set $result $key (concat (get $dst $key) $value) }}
                {{- else if (eq (kindOf $value) "map") }}
                    {{- /* recurse in that case */}}
                    {{- $_ := tuple (get $dst $key) $value | include "merge-append" }}
                {{- else }}
                    {{- /* we can't merge other kind of values like string or integers, overwrite dst value with src one */}}
                    {{- /* NOTE: maybe we should have a merge-append and merge-overwrite-append to choose whether we should keep value from dst or not */}}
                    {{- $_ := set $result $key $value }}
                {{- end }}
            {{- else }}
                {{- /* we can't merge two different kind of values, overwrite dst value with src one */}}
                {{- /* NOTE: maybe we should have a must-merge-append variant that would fail in that case? of fail inconditionally? */}}
                {{- $_ := set $result $key $value }}
            {{- end }}
        {{- else }}
            {{- /* key not in dst, merge it from src */}}
            {{- $_ := set $result $key $value }}
        {{- end }}
    {{- end }}
    {{- $dst := $result }}
{{- end }}


{{/*
Define RKE2 debug aliases for node as a list of commands.

Usage:

    tuple "cp" | include "rke2-alias-commands"
*/}}
{{- define "rke2-alias-commands" -}}
{{- $node_type := index . 0 -}}
- echo 'alias ctr="/var/lib/rancher/rke2/bin/ctr --namespace k8s.io --address /run/k3s/containerd/containerd.sock"' >> /root/.bashrc
- echo 'alias crictl="/var/lib/rancher/rke2/bin/crictl --runtime-endpoint /run/k3s/containerd/containerd.sock"' >> /root/.bashrc
{{- if (eq $node_type "md") }} {{/* NOTE: MD node kubeconfig has restricted RBAC */}}
- echo 'alias kubectl="KUBECONFIG=/var/lib/rancher/rke2/agent/kubelet.kubeconfig /var/lib/rancher/rke2/bin/kubectl"' >> /root/.bashrc
{{- else if (eq $node_type "cp") }}
- echo 'alias kubectl="KUBECONFIG=/etc/rancher/rke2/rke2.yaml /var/lib/rancher/rke2/bin/kubectl"' >> /root/.bashrc
{{- end }}
{{- end }}


{{/*
Define Kubeadm debug aliases for node as a list of commands.

Usage:

    tuple "md" | include "kubeadm-alias-commands"
*/}}
{{- define "kubeadm-alias-commands" -}}
{{- $node_type := index . 0 -}}
{{- if (eq $node_type "md") }} {{/* NOTE: MD node kubeconfig has restricted RBAC */}}
- echo 'alias kubectl="KUBECONFIG=/etc/kubernetes/kubelet.conf kubectl"' >> /root/.bashrc
{{- else if (eq $node_type "cp") }}
- echo 'alias kubectl="KUBECONFIG=/etc/kubernetes/admin.conf kubectl"' >> /root/.bashrc
{{- end }}
{{- end }}


{{/*
Check the Linux distributor is openSUSE and configure DNS in /etc/sysconfig/network/config_path.

Usage:

    tuple $assign_dns_from_dhcp $dns_servers | include "shell-opensuse-dns"

- whenever $assign_dns_from_dhcp is equal to string "true", then only "netconfig update -f" is used, while NETCONFIG_DNS_POLICY is set to the (default) value "auto"; see https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster/-/merge_requests/199#note_1677434793,
- whenever $assign_dns_from_dhcp is not equal to string "true", then NETCONFIG_DNS_STATIC_SERVERS and NETCONFIG_DNS_POLICY are set to the values of $dns_servers and "STATIC", respectively.
*/}}
{{- define "shell-opensuse-dns" -}}
{{- $assign_dns_from_dhcp := index . 0 -}}
{{- $dns_servers:= index . 1 -}}

OS_DISTRIBUTOR=$(lsb_release -i | awk -F ' ' '{print $3}')
if [ "${OS_DISTRIBUTOR}" = "openSUSE" ] || [ "${OS_DISTRIBUTOR}" = "SUSE" ]; then
  ASSIGN_DNS_FROM_DHCP={{ $assign_dns_from_dhcp }}
  # for SUSE OS, the NETCONFIG_DNS_STATIC_SERVERS needs to be of form "1.1.1.1 8.8.8.8 8.8.4.4" if more IPs are to be used
  DNS_STATIC_SERVERS="{{ $dns_servers | join " " }}"
  DNS_POLICY="auto"
  if [ ! "${ASSIGN_DNS_FROM_DHCP}" = "true" ]; then
    DNS_POLICY="STATIC"
    sed -i "s/NETCONFIG_DNS_STATIC_SERVERS=.*/NETCONFIG_DNS_STATIC_SERVERS=\"${DNS_STATIC_SERVERS}\"/" /etc/sysconfig/network/config
  fi
    sed -i "s/NETCONFIG_DNS_POLICY=.*/NETCONFIG_DNS_POLICY=${DNS_POLICY}/" /etc/sysconfig/network/config
    netconfig update -f
fi
{{- end -}}


{{/*
Check the air_gapped RKE2 version matches the requested one and the present of rke2_artifacts
*/}}
{{- define "check-rke2-version" -}}
if [ -d /opt/rke2-artifacts ]; then
  tar -zxvf /opt/rke2-artifacts/rke2.linux-amd64.tar.gz bin/rke2
  rke2version=$(./bin/rke2 -v | head -1 | cut -d " " -f 3)
  if [ "$rke2version" != "{{ .Values.k8s_version }}" ]; then
    {{- if .Values.air_gapped  }}
    echo "ERROR: Local RKE2 version $rke2version is different than the expected {{ .Values.k8s_version }}" && exit 1
    {{- else }}
    echo "WARNING: Ignoring local RKE2 artifacts because local version $rke2version is different than the expected {{ .Values.k8s_version }}"
    mv /opt/rke2-artifacts/rke2.linux-amd64.tar.gz /opt/rke2-artifacts/rke2-wrong-version.linux-amd64.tar.gz
    rm -rf bin/rke2
    {{- end  }}
  else
    echo "exporting INSTALL_RKE2_ARTIFACT_PATH so that RKE2 installation will be done from binaries in /opt/rke2-artifacts"
    export INSTALL_RKE2_ARTIFACT_PATH=/opt/rke2-artifacts
  fi
fi
{{- end -}}


{{/*

"control-plane-rollout-strategy" Named template

Produces (kubeadmcontrolplane|rke2controlplane).spec.rolloutStrategy

*/}}
{{- define "control-plane-rollout-strategy" -}}
{{- $rolloutStrategy := .Values.control_plane.rolloutStrategy | default dict -}}
{{/* for baremetal, we default maxSurge to 0, to have out-of-the-box support for rolling update without spare hardware */}}
{{/* (we don't do this if control_plane_replicas is 1, it would not make sense) */}}
{{- if (and (not $rolloutStrategy) (.Values.capi_providers.infra_provider | eq "capm3") (not (.Values.control_plane_replicas | int | eq 1))) -}}
  {{- $rolloutStrategy = dict "type" "RollingUpdate" "rollingUpdate" (dict "maxSurge" 0) -}}
{{- end -}}
{{- if $rolloutStrategy }}
rolloutStrategy: {{ $rolloutStrategy | toYaml | nindent 2 -}}
{{- end -}}
{{- end -}}


{{/*
capo-image

Helper to produce the image/imageUUID specification for a CAPO OpenStackMachineTemplate.spec.template.spec

If reads a dict correponding to merging the different levels
of .capo .control_plane.capo .machine_deployment_defaults.capo, .machine_deploymets.*.capo
and produces either 'image' or 'imageUUID'.

- "imageUUID" is produced if the passed dict contains "image_key", in that case the UUID is read from .Values.os_images.<image_key>.openstack_glance_uuid
  - an error is raised if ".Values.os_images" has no key equal to "image_key", or if the dict for that key has no "openstack_glance_uuid" key
- or else "image" is produced if the passed dict contains "image_name"
- an error is raised if the dict contains none of these

It is called, for instance:

{{ include "capo-image" (tuple $envAll ".capo/.control_plane.capo" (mergeOverwrite (deepcopy .Values.capo) (deepcopy .Values.control_plane.capo))) }}

                                        ^ only for error messages

which would produce:

  image: my-image

or:

  imageUUID: <openstack-image-uuid>

*/}}
{{- define "capo-image" -}}
  {{- $envAll := index . 0 -}}
  {{- $context := index . 1 -}}{{/* this is just used to display useful error messages */}}
  {{- $def := index . 2 -}}
  {{- if and ($def.image_key | default "") ($def.image_name | default "") -}}
    {{- fail (printf "%s: both .image_key and .image_name are set or inherited, need to choose one (set the other to '')" $context) -}}
  {{- end -}}
  {{- if ($def.image_key | default "") -}}
    imageUUID: {{ $envAll.Values.os_images
                    | dig $def.image_key ""
                    | required (printf "%s: nothing in os_images for image_key %s" $context $def.image_key)
                    | dig "openstack_glance_uuid" ""
                    | required (printf "%s: no openstack_glance_uuid in os_images.%s" $context $def.image_key) -}}
  {{- else -}}
    image: {{ $def | dig "image_name" "" | required "%s: neither image_key or image_name was specified" -}}
  {{- end -}}
{{- end -}}


{{/*
transform_dict_to_list: Transform a dict (key: values )in a list of item (key=value)

Use this function, when the target field is a list and you want the values in your helm chart to be a dict.
This will allow better override of helm chart value and benefit from the functionnality of mergeOverwrite.

Usage:

    $source_list | include "transform_dict_to_list" | fromYamlArray

If dict has key

Values:

    source_list:
      one: thing
      two: stuff

Template:

{{- tuple $source_list | include "transform_dict_to_list" | fromYamlArray }}

Result:

    - one=thing
    - two=stuff

If dict is empty

Values:

    source_list: {}

Template:

{{- tuple $source_list | include "transform_dict_to_list" | fromYamlArray }}


*/}}

{{- define "transform_dict_to_list" }}
{{- $current_list := list -}}
{{ range $dict_key, $dict_value := . }}
{{ $current_list = append $current_list (printf "%s=%v" $dict_key $dict_value) }}
{{ end }}

{{- $current_list | toYaml -}}

{{- end }}

{{/*
capm3 image details

Helper to produce the Metal3MachineTemplate.spec.template.spec.image specification
*/}}

{{- define "capm3-image" -}}
  {{- $envAll := index . 0 -}}
  {{- $context := index . 1 -}}{{/* this is just used to display useful error messages */}}
  {{- $def := index . 2 -}}
  {{- if and ($def.image_key | default "") ($def.machine_image_url | default "") -}}
    {{- fail (printf "%s: both .image_key and .machine_image_url are set or inherited, need to choose one (set the other to '')" $context) -}}
  {{- end -}}
  {{- if ($def.image_key | default "") -}}
    {{- $os_image_info := $envAll.Values.os_images | dig $def.image_key "" | required (printf "%s: nothing in os_images for image_key %s" $context $def.image_key) }}
    {{- $base_os_image_server_url := ternary ( printf "http://os-image-server-%s.os-images.svc.cluster.local:8080" $def.image_key ) ( printf "http://%s" ($envAll.Values.capm3.image_provisioning_host | default $envAll.Values.mgmt_cluster_ip) ) $envAll.Values.capm3.use_os_image_server_service_urls -}}
checksum: {{ $os_image_info | dig "sha256" "" | required (printf "%s: no sha256 in os_images.%s" $context $def.image_key) }}
checksumType: sha256
format: {{ $os_image_info | dig "image-format" "" | required (printf "%s: no image-format in os_images.%s" $context $def.image_key) }}
url: {{ $base_os_image_server_url }}/{{ $os_image_info | dig "filename" "" | required (printf "%s: no filename in os_images.%s" $context $def.image_key) }}
  {{- else -}}
checksum: {{ $def.machine_image_checksum }}
checksumType: {{ $def.machine_image_checksum_type }}
format: {{ $def.machine_image_format }}
url: {{ $def.machine_image_url }}
  {{- end -}}
{{- end -}}

{{/*
Define the source for the <customized default disks JSON> Longhorn configuration depending on infrastructure provider
and annotate node with `node.longhorn.io/default-disks-config: <customized default disks JSON>`
depending on bootstrap provider and role, and label node with `node.longhorn.io/create-default-disk: config`,
only for Kubeadm bootstrap provider, due to the fact that in RKE2 templates we've got

  cluster:
    rke2:
      nodeLabels: {}

    control_plane:
      rke2:
        nodeLabels: {}

    machine_deployment_default:
      rke2:
        nodeLabels: {}

    machine_deployments:
      md0:
        rke2:
          nodeLabels: {}

available as chart values that could provide different values, so RKE2ControlPlane.spec.agentConfig.nodeAnnotations
native way of setting node annotations could collide with these cloud-init `kubectl label node $(hostname)` commands

Usage:

    tuple .Values.capi_providers.infra_provider .Values.capi_providers.bootstrap_provider "cp" | include "shell-longhorn-node-metadata"

Result:

    sylva_longhorn_disks_base64e="{{`{{ds.meta_data.sylva_longhorn_disks}}`}}"  # only for capm3
    sylva_longhorn_disks=$(echo "$sylva_longhorn_disks_base64e" | base64 -d)    # only for capm3
    kubectl annotate node $(hostname) node.longhorn.io/default-disks-config="$sylva_longhorn_disks"
    kubectl label node $(hostname) node.longhorn.io/create-default-disk=config  # only for cabpk

Notes:
- the $sylva_longhorn_disks contents varies depending on the infra_provider, at this time it's only implemented for CAPM3

- for CABPR CP nodes to run `kubectl` during cloud-init (/var/lib/cloud/instance/scripts/runcmd) we use:
    /var/lib/rancher/rke2/bin/kubectl --kubeconfig /etc/rancher/rke2/rke2.yaml annotate node $(hostname) node.longhorn.io/default-disks-config="$sylva_longhorn_disks"
  and for CABPR MD nodes:
    /var/lib/rancher/rke2/bin/kubectl --kubeconfig /var/lib/rancher/rke2/agent/kubelet.kubeconfig annotate node $(hostname) node.longhorn.io/default-disks-config="$sylva_longhorn_disks"

- for CABPK CP nodes:
    kubectl --kubeconfig /etc/kubernetes/admin.conf annotate node $(hostname) node.longhorn.io/default-disks-config="$sylva_longhorn_disks"
    kubectl --kubeconfig /etc/kubernetes/admin.conf label node $(hostname) node.longhorn.io/create-default-disk=config
  and for CABPK MD nodes:
    kubectl --kubeconfig /etc/kubernetes/kubelet.conf annotate node $(hostname) node.longhorn.io/default-disks-config="$sylva_longhorn_disks"
    kubectl --kubeconfig /etc/kubernetes/kubelet.conf label node $(hostname) node.longhorn.io/create-default-disk=config

*/}}
{{- define "shell-longhorn-node-metadata" -}}
{{- $infra_provider := index . 0 -}}
{{- $bootstrap_provider := index . 1 -}}
{{- $node_type := index . 2 -}}

  {{- if (eq $infra_provider "capm3") }}
sylva_longhorn_disks_base64e="{{`{{ds.meta_data.sylva_longhorn_disks}}`}}"
sylva_longhorn_disks=$(echo "$sylva_longhorn_disks_base64e" | base64 -d)
  {{- else if (not (eq $infra_provider "capd")) }}
sylva_longhorn_disks="{{ printf "Not implemented for %s, please add storage devices, mount them and annotate node manually" (upper $infra_provider) }}"
  {{- else if (eq $infra_provider "capd") }}
sylva_longhorn_disks="No disks can be added to CAPD node"
  {{- end }}

  {{- $kubectl_command := "" }}
  {{- if (eq $bootstrap_provider "cabpr") }}
    {{- if (eq $node_type "cp") }}
      {{- $kubectl_command = "/var/lib/rancher/rke2/bin/kubectl --kubeconfig /etc/rancher/rke2/rke2.yaml" -}}
    {{- else if (eq $node_type "md") }}
      {{- $kubectl_command = "/var/lib/rancher/rke2/bin/kubectl --kubeconfig /var/lib/rancher/rke2/agent/kubelet.kubeconfig" -}}
    {{- end }}
  {{- else if (eq $bootstrap_provider "cabpk") }}
    {{- if (eq $node_type "cp") }}
      {{- $kubectl_command = "kubectl --kubeconfig /etc/kubernetes/admin.conf" -}}
    {{- else if (eq $node_type "md") }}
      {{- $kubectl_command = "kubectl --kubeconfig /etc/kubernetes/kubelet.conf" -}}
    {{- end }}
  {{- end }}
{{/* Wait for nodes prior to annotate them, we should use "kubectl wait --wait-for-creation" instead once https://github.com/kubernetes/kubernetes/pull/122994 will land */}}
max_wait_secs=300
start_time=$(date +%s)
while true; do
  current_time=$(date +%s)
  if [ `expr $current_time - $start_time` -gt $max_wait_secs ]; then
    echo "Waited for node \"$(hostname)\" to exist for $max_wait_secs seconds without luck. Returning with error."
    exit 1
  fi
  if {{ $kubectl_command }} get node $(hostname) --request-timeout "5s" > /dev/null 2>&1; then
    break
  else
    sleep 2
  fi
done
{{ printf "%s label --overwrite node $(hostname) node.longhorn.io/create-default-disk=config" $kubectl_command }}
{{ printf "%s annotate --overwrite node $(hostname) node.longhorn.io/default-disks-config=\"$sylva_longhorn_disks\"" $kubectl_command }}
{{- end -}}

{{/* Define "rancher_dns_resolver" to managment cluster IP if not already set. */}}
{{- define "rancher_dns_resolver" -}}
  {{- if .Values.rancher_dns_resolver -}}
    {{- .Values.rancher_dns_resolver -}}
  {{- else -}}
    {{- .Values.mgmt_cluster_ip -}}
  {{- end -}}
{{- end -}}


{{/*

"in-resource-groups"

used to check if a "group" of resources is present in .Values.resource_groups

returns 'true' if the parameter is in .Values.resource_groups or if 'all' is in .Values.resource_groups
else returns ''

usage:

  {{ if (tuple . "baremetal-hosts" | include "in-resource-groups") }}
  ...


*/}}
{{- define "in-resource-groups" -}}
  {{- $envAll := index . 0 }}
  {{- $group := index . 1 }}
  {{- if $envAll.Values.resource_groups | has "all" -}}
  true
  {{- else if $envAll.Values.resource_groups | has $group -}}
  true
  {{- else -}}
  {{/* return an empty string, which will evaluate as false */}}
  {{- end -}}
{{- end -}}
