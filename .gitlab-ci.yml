---
stages:
  - test
  - deploy

default:
  image: registry.gitlab.com/sylva-projects/sylva-elements/container-images/ci-image:v1.0.26

workflow:
  rules:
    - when: always

variables:
  GITLEAKS_ARGS: '--verbose --log-opts=$CI_COMMIT_SHA'
  SYLVA_CORE_BRANCH:
    value: main
    description: "Sylva-core branch to use for cross-repo pipeline"

include:
  - project: "to-be-continuous/gitleaks"
    ref: 2.5.2
    file: "templates/gitlab-ci-gitleaks.yml"
  - project: 'renovate-bot/renovate-runner'
    ref: v17.237.0
    file: '/templates/renovate-config-validator.gitlab-ci.yml'
    rules:
      - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
        changes:
          paths:
            - .gitlab-ci.yml
            - renovate.json
  - project: 'sylva-projects/sylva-elements/renovate'
    ref: 1.0.0
    file: '/templates/renovate-dry-run.gitlab-ci.yml'
    rules:
      - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
        changes:
          paths:
            - renovate.json

.lint-base:
  stage: test
  needs: []
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

avoid-typo-on-bootstrap:
  extends: .lint-base
  script:
    - |
      rm -rf .git  # because busybox grep does not support --exclude-dir
      echo "Check against frequent typos on 'bootstrap'..."
      set +e
      typos=$(grep -rnsiE 'boostrap|bootrap|bootsrap' . | grep -v '.gitlab-ci.yaml:      typos=')
      set -e
      if [ -n "$typos" ]; then
        echo "A few typos were found on the 'bootstrap' word:"
        echo "-----------------"
        echo "$typos"
        echo "-----------------"
        exit 1
      fi

check-docs-markdown:
  extends: .lint-base
  image: registry.gitlab.com/gitlab-org/gitlab-docs/lint-markdown:alpine-3.16-vale-2.20.2-markdownlint-0.32.2-markdownlint2-0.5.1
  script:
    - git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - |
      md_files=$(git diff --name-only $CI_COMMIT_SHA origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME | grep "\.md$" || true)
      if [ -n "$md_files" ] ; then
        markdownlint-cli2-config .gitlab/configuration/.markdownlint.yml $md_files
      else
        echo "No modified .md files"
      fi
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      changes:
        - "**/*.md"

helm-lint:
  extends: .lint-base
  script:
    - helm lint . --values test-values/base/values.yaml

helm-yamllint:
  extends: .lint-base
  script:
    - yamllint . -d "$(cat < .gitlab/configuration/yamllint.yaml) $(cat < .gitlab/configuration/yamllint-helm-exclude-chart-templates.yaml)"

helm-template-yamllint:
  extends: .lint-base
  script:
    - .gitlab/scripts/helm-template-yamllint.sh
  variables:
    HELM_NAME: sylva-capi-cluster

helm-schema-validation:
  extends: .lint-base
  script:
    - .gitlab/scripts/helm-schema-validation.sh
  variables:
    HELM_NAME: sylva-capi-cluster

# Cross repo pipeline is manual as only maintainers of Sylva-core can run it
sylva-core-cross-project:
  stage: deploy
  variables:
    REMOTE_VALUES: >
      source_templates:
        sylva-capi-cluster:
          kind: GitRepository
          spec:
            url: https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster.git
            ref:
              commit: $CI_COMMIT_SHA
  trigger:
    project: sylva-projects/sylva-core
    branch: $SYLVA_CORE_BRANCH
    strategy: depend
  when: manual
  allow_failure: true
