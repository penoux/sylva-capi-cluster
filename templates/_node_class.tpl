{{/*
get_node_class_argument
Output arguments's definition as a YAML dict of the named node class
*/}}
{{- define "get_node_class_argument" }}
{{- $envAll := index . 0 -}}
{{- $argument := index . 1 -}}
{{- $node_class_name := index . 2 -}}

{{- $target_node_class_name := printf "%s" ($node_class_name | default $envAll.Values.default_node_class)  -}}

{{- if hasKey $envAll.Values.node_classes $target_node_class_name }}
    {{- $current_node_class := get $envAll.Values.node_classes $target_node_class_name -}}

    {{- if hasKey $current_node_class $argument }}
        {{- $current_node_class_key := get $current_node_class $argument -}}
        {{- $current_node_class_key | toYaml -}}

    {{- else }}
    {{- printf "DEBUG: node class name '%s' has no key '%s'" $target_node_class_name $argument | fail }}
    {{- end }}

{{- else }}
  {{- printf "DEBUG: node class name '%s' doesn't exist" $target_node_class_name | fail }}
{{- end }}
{{- end }}

{{/*
get_node_class_grub_command
Update grub to configure hugepage and add extra option to GRUB_CMDLINE_LINUX
*/}}
{{- define "get_node_class_grub_command" -}}
{{- $envAll := index . 0 -}}
{{- $node_class_name := index . 1 -}}
{{- $grub := tuple $envAll "kernel_cmdline" $node_class_name | include "get_node_class_argument" | fromYaml -}}
{{/* Get current grub by merging values grub and default grub*/}}
{{- $default_grub := dict "extra_options" "" "hugepages" (dict "enabled" false "2M_percentage_total" 0 "1G_percentage_total" 0 "default_size" "2M") -}}
{{- $current_grub := mergeOverwrite $default_grub $grub -}}
{{/* set used variables*/}}
{{- $hugepages_2M_percentage_total := get (get $current_grub "hugepages") "2M_percentage_total" -}}
{{- $hugepages_1G_percentage_total := get (get $current_grub "hugepages") "1G_percentage_total" -}}
{{- $hugepages_default_size := get (get $current_grub "hugepages") "default_size" -}}
{{- $hugepages_enabled := get (get $current_grub "hugepages") "enabled" -}}
{{- $grub_extra_options := $current_grub.extra_options -}}
{{/* Make sure that 10% of memory is not hugepage and enabled*/}}
{{- if $hugepages_enabled -}}
  {{- if gt (addf $hugepages_2M_percentage_total $hugepages_1G_percentage_total) 0.9 -}}
  {{- printf "DEBUG: node class name '%s' has more than 90 percent of memory allocated to hugepage, hugepage_2M:'%s' hugepage_1G'%s'" $node_class_name $hugepages_2M_percentage_total $grub.hugepages_1G_percentage_total | fail -}}
  {{- end -}}
{{- else -}}
{{- $hugepages_2M_percentage_total = 0 -}}
{{- $hugepages_1G_percentage_total = 0 -}}
{{- end -}}
{{/*  Only reboot if HugePages is enabled or Kernel has extra options */}}
{{- if or (eq $hugepages_enabled true) (ne $grub_extra_options "") -}}
if [ ! -f "/var/lib/grub-init" ]; then
  touch "/var/lib/grub-init"
  total_ram=$(awk '/MemTotal/ { printf "%.0f", $2/1024 }' /proc/meminfo)

  number_hugepage_1G=$(printf %.0f "$(awk 'BEGIN {a='$total_ram'; b={{ $hugepages_1G_percentage_total }}; c=1024; z=a*b/c; print z}')")
  number_hugepage_2M=$(printf %.0f "$(awk 'BEGIN {a='$total_ram'; b={{ $hugepages_2M_percentage_total }}; z=a*b/2; print z}')")
  next_grub="{{ $grub_extra_options }} hugepagesz=1G hugepages=$number_hugepage_1G hugepagesz=2M hugepages=$number_hugepage_2M default_hugepagesz={{ $hugepages_default_size }} transparent_hugepage=never"

  sed -i -e '/GRUB_TIMEOUT=5/ d' /etc/default/grub
  current_grub_cmdline_linux_default=$(grep '^GRUB_CMDLINE_LINUX_DEFAULT=' /etc/default/grub | tail -1 | sed -e "s/^[^=]*=[\"']\\?//" -e "s/['\"]$//")
  sed -i -e '/^GRUB_CMDLINE_LINUX_DEFAULT=/ d' /etc/default/grub
  echo "GRUB_RECORDFAIL_TIMEOUT=1" >> /etc/default/grub
  echo "GRUB_CMDLINE_LINUX_DEFAULT=\"$current_grub_cmdline_linux_default $next_grub\"" >> /etc/default/grub

  if type grub2-mkconfig >/dev/null; then
    GRUB_MKCONFIG="grub2-mkconfig"
  else
    GRUB_MKCONFIG="grub-mkconfig"
  fi
  if [ -d /boot/grub2 ]; then
    GRUB_CFG=/boot/grub2/grub.cfg
  else
    GRUB_CFG=/boot/grub/grub.cfg
  fi
  $GRUB_MKCONFIG -o $GRUB_CFG

  echo "cloud-init clean issued at: $(date -Iseconds)"
  cloud-init clean --reboot
  sleep 86400  # ensure that this script does not finish before the reboot is effective, to make sure that following modules won't be executed by cloud-init before it receives the SIGTERM signal
fi
{{- else -}}
echo "No update" > /var/lib/grub-init
{{- end -}}
{{- end }}
