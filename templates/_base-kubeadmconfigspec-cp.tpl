{{- define "base-kubeadmConfigSpec-CP" }}
initConfiguration:
  nodeRegistration:
    taints: []
    kubeletExtraArgs: {{ mergeOverwrite (deepCopy .Values.kubelet_extra_args) (deepCopy .Values.control_plane.kubelet_extra_args) | toYaml | nindent 6 }}
joinConfiguration:
  nodeRegistration:
    taints: []
    kubeletExtraArgs: {{ mergeOverwrite (deepCopy .Values.kubelet_extra_args) (deepCopy .Values.control_plane.kubelet_extra_args) | toYaml | nindent 6 }}
  {{- if .Values.etcd }}
clusterConfiguration:
  etcd:
    local:
      extraArgs:
      {{- range $key, $value := .Values.etcd }}
        {{ $key }}: {{ $value | quote }}
      {{ end -}}
  {{- else }}
clusterConfiguration: {}
  {{- end }}
ntp: {{ .Values.ntp | toYaml | nindent 2 }}
preKubeadmCommands:
  {{- if and (not (eq .Values.capi_providers.infra_provider "capd")) .Values.use_custom_rancher_dns_resolver }}
  - systemctl restart systemd-resolved
  {{- end }}
  {{- if .Values.proxies.http_proxy }}
  - systemctl daemon-reload
  - systemctl restart containerd.service
  {{- end }}
  - | {{- include "kubeadm-alias-commands" (tuple "cp") | nindent 2 }}
  - echo "Preparing Kubeadm bootstrap" > /var/log/my-custom-file.log
files:
{{ $kubeadmcpfiles := list }}
{{- if or (eq .Values.capi_providers.infra_provider "capo") (eq .Values.capi_providers.infra_provider "capv") (eq .Values.capi_providers.infra_provider "capm3") }}
    {{- $kubeadmcpfiles = include "kubernetes_kubeadm_vip" . | append $kubeadmcpfiles }}
{{- end }}
{{- if .Values.use_custom_rancher_dns_resolver }}
    {{- $kubeadmcpfiles = include "resolv_conf" . | append $kubeadmcpfiles -}}
{{- end }}
{{- if (.Values.registry_mirrors | dig "hosts_config" "") }}
    {{- $kubeadmcpfiles = include "registry_mirrors" . | append $kubeadmcpfiles  -}}
{{- end }}
{{- if .Values.proxies.http_proxy }}
    {{-  $kubeadmcpfiles = include "containerd_proxy_conf" . | append $kubeadmcpfiles -}}
{{- end }}
{{- $additional_files := mergeOverwrite (deepCopy .Values.additional_files) (deepCopy .Values.control_plane.additional_files) }}
{{- if $additional_files }}
    {{- $kubeadmcpfiles = tuple . $additional_files | include "additional_files" | append $kubeadmcpfiles -}}
{{- end }}
{{- if $kubeadmcpfiles -}}
  {{- range $kubeadmcpfiles -}}
    {{ . | indent 2 }}
  {{- end }}
{{- else }}
    []
{{- end }}
postKubeadmCommands:
  - set -e
  {{- if .Values.enable_longhorn }}
  - | {{ tuple .Values.capi_providers.infra_provider "cabpk" "cp" | include "shell-longhorn-node-metadata" | nindent 4 }}
  {{- end }}
{{- end }}
